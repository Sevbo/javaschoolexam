package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        
        try {
            statement = statement.replace(" ", "");
            if (statement.matches("(.*)--+(.*)")) {return null;}

            String expr;
            // First Step - Work with Parentheses
            do {
                expr = null;
                Pattern p = Pattern.compile("\\(([0-9\\+\\-\\*\\/\\.]+)\\)");
                Matcher m = p.matcher(statement);
                if (m.find()) {
                    expr = m.group(1);
                    Double d = calcExpression(m.group(1));
                    statement = statement.replace("(" + m.group(1) + ")", d.toString());
                }
            } while (expr != null);
            // Calc remain expression
            Double d = calcExpression(statement);
            d = round(d, 4);
            return (d % 1 == 0) ? String.format("%.0f", d) : d.toString();
        } catch(Exception e) {
            return null;
        }
    }

    private Double calcExpression(String inputData) {
        String expr;
        // Calc high priority expressions
        do {
            expr = null;
            Pattern p = Pattern.compile("\\-?[\\d\\.]+[\\*\\/]\\-?[\\d\\.]+");
            Matcher m = p.matcher(inputData);
            if (m.find()) {
                expr = m.group(0);
                Double d = calcPrimitive(m.group(0));
                inputData = inputData.replace(m.group(0), d.toString());
            }
        } while (expr != null);
        // Сalc low priority expressions
        do {
            expr = null;
            Pattern p = Pattern.compile("\\-?[\\d\\.]+[\\+\\-]\\-?[\\d\\.]+");
            Matcher m = p.matcher(inputData);
            if (m.find()) {
                expr = m.group(0);
                Double d = calcPrimitive(m.group(0));
                inputData = inputData.replace(m.group(0), d.toString());
            }
        } while (expr != null);
        return Double.parseDouble(inputData);
    }

    private Double calcPrimitive(String expr) {
        Pattern p = Pattern.compile("(\\-?[\\d\\.]+)([\\+\\-\\*\\/])(\\-?[\\d\\.]+)");
        Matcher m = p.matcher(expr);
        if (m.find()) {
            if ("+".equals(m.group(2))) {
                return Double.parseDouble(m.group(1)) + Double.parseDouble((m.group(3)));
            } else if ("-".equals(m.group(2))) {
                return Double.parseDouble(m.group(1)) - Double.parseDouble((m.group(3)));
            } else if ("/".equals(m.group(2))) {
                return Double.parseDouble(m.group(1)) / Double.parseDouble((m.group(3)));
            } else if ("*".equals(m.group(2))) {
                return Double.parseDouble(m.group(1)) * Double.parseDouble((m.group(3)));
            }
        }
        return 0.0;
    }

    // Some routines

    private double round(double value, int places) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
