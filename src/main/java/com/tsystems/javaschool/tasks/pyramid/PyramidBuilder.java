package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int[][] result;
        int n = inputNumbers.size();
        Double x = (Math.sqrt(1+8*n)-1)/2;
        if (x != Math.floor(x)) {throw new CannotBuildPyramidException();}
        int xi = (int) Math.floor(x);
        int wsize = xi+xi-1;
        int h = wsize / 2;
        try {
            Collections.sort(inputNumbers);
            result = new int[xi][wsize];
            int counter = 0;
            int o = (xi % 2 == 0) ? 1 : 0;
            for (int i = 1; i <= xi; i++) {
                int om = 0;
                for (int j = -h; j <= h; j++) {
                    if ((-i + 1 <= j) && (i - 1 >= j)) {
                        if (om == o) {
                            result[i-1][h+j] = inputNumbers.get(counter);
                            counter++;
                        } else {
                            result[i-1][h+j] = 0;
                        }
                    } else {
                        result[i-1][h+j] = 0;
                    }
                    om++;
                    if (om == 2) {
                        om = 0;
                    }
                }
                o++;
                if (o == 2) {
                    o = 0;
                }
            }
        } catch(Exception e) {throw new CannotBuildPyramidException();}
        return result;
    }
}
