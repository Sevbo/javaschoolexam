package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        int Ystart = 0;
        int fCount = 0;
        if (x == null || y == null) {throw new IllegalArgumentException();}

        for (int i = 0; i < x.size(); i++) {
            for (int j = Ystart; j < y.size(); j++) {
                if (x.get(i).equals(y.get(j))) {
                    Ystart = j + 1;
                    fCount++;
                    break;
                }
            }
        }
        return fCount == x.size();
    }
}
